import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesComponent} from "./components/courses/courses.component";
import {MainPageComponent} from "./components/main-page/main-page.component";
import {AboutComponent} from "./components/about/about.component";
import {FaqComponent} from "./components/faq/faq.component";
import {ArticlesComponent} from "./components/articles/articles.component";

const routes: Routes = [
  {path: "", component: MainPageComponent},
  {path: "courses", component: CoursesComponent},
  {path: "articles", component: ArticlesComponent},
  {path: "about", component: AboutComponent},
  {path: "faq", component: FaqComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
