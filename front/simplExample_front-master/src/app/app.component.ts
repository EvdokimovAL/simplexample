import { Component } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";
import {CrumbsEnum, CrumbType} from "./dto/crumbs.enum";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'simplexample';
  currentLink : string
  breadcrumbs: CrumbType[] = [];
  allCrumbs: CrumbType[] = [
    {
      link: '',
      title: 'Главная',
    },
    {
      link: 'courses',
      title: 'Наши курсы',
    },
    {
      link: 'about',
      title: 'О преподавателе',
    },
    {
      link: 'articles',
      title: 'Статьи',
    },
    {
      link: 'faq',
      title: 'Частые вопросы',
    },
  ];

  constructor(private router: Router) {
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.currentLink = event.url;
          const linksArray = this.currentLink.split('/');
          this.breadcrumbs = this.allCrumbs.filter(crumb => linksArray.includes(crumb.link));
          this.breadcrumbs.pop();
        }
      })
  }
}
