import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { HeaderComponent } from './components/header/header.component';
import { TileComponent } from './components/shared/tile/tile.component';
import { SharedModule } from "./components/shared/shared.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { OrderModalComponent } from './components/shared/order-modal/order-modal.component';
import { CoursesComponent } from './components/courses/courses.component';
import { AboutComponent } from './components/about/about.component';
import { FaqComponent } from './components/faq/faq.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainPageCoursesSectionComponent } from './components/main-page/sections/main-page-courses-section/main-page-courses-section.component';
import { MainPageAboutSectionComponent } from './components/main-page/sections/main-page-about-section/main-page-about-section.component';
import { MainPageFaqSectionComponent } from './components/main-page/sections/main-page-faq-section/main-page-faq-section.component';
import { ArticlesComponent } from './components/articles/articles.component';
import {MainPageArticlesSectionComponent} from "./components/main-page/sections/main-page-articles-section/main-page-articles-section.component";

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    HeaderComponent,
    TileComponent,
    OrderModalComponent,
    CoursesComponent,
    AboutComponent,
    FaqComponent,
    FooterComponent,
    MainPageCoursesSectionComponent,
    MainPageAboutSectionComponent,
    MainPageArticlesSectionComponent,
    MainPageFaqSectionComponent,
    ArticlesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
