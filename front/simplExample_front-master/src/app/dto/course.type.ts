import {DifficultyEnum} from "./difficulty.enum";

export type CourseType = {
  name: string;
  description: string;
  price: number;
  plan: string[];
  composition: string[];
  difficulty: DifficultyEnum;
  subDesc: string;
}
export type ModalOpenEmit = {
  isOpened: boolean;
  data?: CourseType;
}
