export type ArticleType = {
  title: string;
  text: string;
  author: string;
  imgSource: string;
}
