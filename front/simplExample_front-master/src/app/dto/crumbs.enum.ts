export enum CrumbsEnum {
  '' = 'Главная',
  'articles' = 'Статьи',
}
export type CrumbType = {
  link: string;
  title: string;
}
