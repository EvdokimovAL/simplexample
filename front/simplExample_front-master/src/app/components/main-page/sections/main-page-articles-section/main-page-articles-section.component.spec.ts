import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageAboutSectionComponent } from './main-page-articles-section.component';

describe('MainPageAboutSectionComponent', () => {
  let component: MainPageAboutSectionComponent;
  let fixture: ComponentFixture<MainPageAboutSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPageAboutSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageAboutSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
