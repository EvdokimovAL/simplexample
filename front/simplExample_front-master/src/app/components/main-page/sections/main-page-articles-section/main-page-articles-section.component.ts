import { Component, OnInit } from '@angular/core';
import {ArticlesService} from "../../../articles/articles.service";
import {ArticleType} from "../../../../dto/article.type";

@Component({
  selector: 'app-main-page-articles-section',
  templateUrl: './main-page-articles-section.component.html',
  styleUrls: ['./main-page-articles-section.component.sass']
})
export class MainPageArticlesSectionComponent implements OnInit {
  articles: ArticleType[];

  constructor(private articlesService: ArticlesService) {
    this.articles = articlesService.articles;
  }

  ngOnInit(): void {
  }

}
