import { Component, OnInit } from '@angular/core';
import {DifficultyEnum} from "../../../../dto/difficulty.enum";
import {CoursesService} from "../../../courses/courses.service";
import {CourseType} from "../../../../dto/course.type";

@Component({
  selector: 'app-main-page-courses-section',
  templateUrl: './main-page-courses-section.component.html',
  styleUrls: ['./main-page-courses-section.component.sass']
})
export class MainPageCoursesSectionComponent implements OnInit {
  Difficulty = DifficultyEnum;
  courses: CourseType[];

  constructor(private coursesService: CoursesService) {
    this.courses = coursesService.courses;
  }

  ngOnInit(): void {
  }

}
