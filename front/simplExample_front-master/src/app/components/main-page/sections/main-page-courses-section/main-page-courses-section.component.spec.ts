import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageCoursesSectionComponent } from './main-page-courses-section.component';

describe('MainPageCoursesSectionComponent', () => {
  let component: MainPageCoursesSectionComponent;
  let fixture: ComponentFixture<MainPageCoursesSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPageCoursesSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageCoursesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
