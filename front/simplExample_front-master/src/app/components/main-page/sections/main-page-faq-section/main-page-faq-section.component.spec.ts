import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageFaqSectionComponent } from './main-page-faq-section.component';

describe('MainPageFaqSectionComponent', () => {
  let component: MainPageFaqSectionComponent;
  let fixture: ComponentFixture<MainPageFaqSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPageFaqSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageFaqSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
