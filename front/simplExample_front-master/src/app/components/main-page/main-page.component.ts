import { Component, OnInit } from '@angular/core';
import {DifficultyEnum} from "../../dto/difficulty.enum";
import {OrderModalService} from "../shared/order-modal/order-modal.service";
import {ModalOpenEmit} from "../../dto/course.type";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent implements OnInit {

  isModalOpened = false;

  constructor(private orderModalService: OrderModalService) {
    this.orderModalService.modalOpenMoment.subscribe((event: ModalOpenEmit) => {
      this.isModalOpened = event.isOpened;
    })
  }

  ngOnInit(): void {
  }

  order(): void {
    this.orderModalService.order();
  }

  scrollToElement(id: string): void {
    const element = document.getElementById(id)
    element?.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
  }
}
