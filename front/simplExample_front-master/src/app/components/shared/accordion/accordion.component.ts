import { AfterContentInit, Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { AccordionGroupComponent } from './accordion-group/accordion-group.component';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.styl']
})
export class AccordionComponent implements AfterContentInit {

  @ContentChildren(AccordionGroupComponent) private groups: QueryList<AccordionGroupComponent>;

  constructor() { }

  ngAfterContentInit(): void {
    this.groups.toArray()[0].opened = true;
    this.groups.toArray().forEach((group) => {
      group.toggle.subscribe(() => this.openGroup(group));
    });
  }

  public openGroup(group: AccordionGroupComponent) {
    this.groups.forEach(gr => gr.opened = false)
    group.opened = true;
  }
}
