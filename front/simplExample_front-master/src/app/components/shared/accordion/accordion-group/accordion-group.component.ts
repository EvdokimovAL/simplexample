import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-accordion-group',
  templateUrl: './accordion-group.component.html',
  styleUrls: ['./accordion-group.component.sass'],
  animations: [
    trigger('smoothCollapse', [
      state('initial', style({
        height: '0',
        opacity: '0',
        overflow: 'hidden',
        visibility: 'hidden'
      })),
      state('final', style({
        overflow: 'hidden'
      })),
      transition('initial<=>final', animate('250ms'))
    ]),
    trigger('arrowRotate', [
      state('default', style({
        transform: 'rotate(0)'
      })),
      state('rotated', style({
        transform: 'rotate(45deg)'
      })),
      transition('default<=>rotated', animate('250ms'))
    ])
  ]
})
export class AccordionGroupComponent {
  @Input() opened = false;
  @Input() title!: string;

  @Output() toggle: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
}
