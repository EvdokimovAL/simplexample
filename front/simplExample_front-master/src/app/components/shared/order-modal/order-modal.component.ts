import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderModalService} from "./order-modal.service";

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.sass']
})
export class OrderModalComponent implements OnInit, OnDestroy {
  alive$: boolean;
  name: string | undefined;
  description: string | undefined;
  price: number | undefined;
  plan: string[] | undefined;
  composition: string[] | undefined;

  constructor(private orderModalService: OrderModalService) {
    this.alive$ = true;
    this.name = this.orderModalService.name;
    this.description = this.orderModalService.description;
    this.price = this.orderModalService.price;
    this.plan = this.orderModalService.plan;
    this.composition = this.orderModalService.composition;
  }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.orderModalService.modalOpenMoment.emit({isOpened: false});
  }

  order(): void {
    this.orderModalService.order();
  }

  ngOnDestroy(): void {
    this.alive$ = false;
  }
}
