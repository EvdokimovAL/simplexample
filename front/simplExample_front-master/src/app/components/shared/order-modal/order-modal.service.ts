import { Injectable, EventEmitter } from '@angular/core';
import {ModalOpenEmit} from "../../../dto/course.type";

@Injectable({
  providedIn: 'root'
})
export class OrderModalService {
  isModalOpened = false;
  modalOpenMoment = new EventEmitter<ModalOpenEmit>();

  name: string | undefined;
  description: string | undefined;
  price: number | undefined;
  plan: string[] | undefined;
  composition: string[] | undefined;

  constructor() {
    this.modalOpenMoment.subscribe((event: ModalOpenEmit) => {
      this.setModalOpen(event.isOpened);

      if (event.isOpened) {
        this.name = event.data?.name;
        this.description = event.data?.description;
        this.price = event.data?.price;
        this.plan = event.data?.plan;
        this.composition = event.data?.composition;
      }
    })
  }


  setModalOpen(value: boolean) {
    this.isModalOpened = value;
  }

  order(): void {
    console.log('order')
  }
}
