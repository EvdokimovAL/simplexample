import {Component, Input, OnInit} from '@angular/core';
import {DifficultyEnum} from "../../../dto/difficulty.enum";
import {OrderModalService} from "../order-modal/order-modal.service";

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.sass']
})
export class TileComponent implements OnInit {
  @Input()
  title!: string;

  @Input()
  description!: string;

  @Input()
  subDesc!: string;

  @Input()
  price!: number;

  @Input()
  plan!: string[];

  @Input()
  composition!: string[];

  @Input()
  difficulty!: DifficultyEnum;

  Difficulty = DifficultyEnum;

  constructor(private orderModalService: OrderModalService) { }

  ngOnInit(): void {
  }

  openModal() {
    this.orderModalService.modalOpenMoment.emit({isOpened: true, data: {
        name: this.title,
        description: this.description,
        price: this.price,
        plan: this.plan,
        composition: this.composition,
        subDesc: this.subDesc,
        difficulty: this.difficulty
    }});
  }

}
