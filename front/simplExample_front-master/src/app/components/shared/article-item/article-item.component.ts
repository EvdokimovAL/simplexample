import {Component, Input, OnInit} from '@angular/core';
import {ArticleType} from "../../../dto/article.type";

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.sass']
})
export class ArticleItemComponent implements OnInit {
  @Input()
  fullPage: boolean = false;
  @Input()
  article: ArticleType;

  constructor() { }

  ngOnInit(): void {
  }

}
