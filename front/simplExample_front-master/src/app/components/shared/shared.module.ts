import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { AccordionComponent } from './accordion/accordion.component';
import { AccordionGroupComponent } from './accordion/accordion-group/accordion-group.component';
import { ArticleItemComponent } from './article-item/article-item.component';

@NgModule({
  declarations: [
    AccordionComponent,
    AccordionGroupComponent,
    ArticleItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
    exports: [
        AccordionComponent,
        AccordionGroupComponent,
        ArticleItemComponent
    ]
})
export class SharedModule {
}
