import { Component, OnInit } from '@angular/core';
import {CoursesService} from "./courses.service";
import {CourseType} from "../../dto/course.type";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.sass']
})
export class CoursesComponent implements OnInit {
  courses: CourseType[];

  constructor(private coursesService: CoursesService) {
    this.courses = coursesService.courses;
  }

  ngOnInit(): void {
  }

}
