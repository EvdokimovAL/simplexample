import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  currentLink : string

  constructor(private router: Router) {
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.currentLink = event.url;
        }
      })
  }

  ngOnInit(): void {
  }

  scrollToElement(id: string): void {
    const element = document.getElementById(id)
    element?.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
  }

}
