import { Component, OnInit } from '@angular/core';
import {ArticleType} from "../../dto/article.type";
import {ArticlesService} from "./articles.service";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.sass']
})
export class ArticlesComponent implements OnInit {
  articles: ArticleType[];

  constructor(private articlesService: ArticlesService) {
    this.articles = articlesService.articles;
  }

  ngOnInit(): void {
  }
}
